package com.example.cuncis.project1;

import android.graphics.drawable.TransitionDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imgTransitionLight;
    Button btnOnOff;
    boolean turnOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgTransitionLight = findViewById(R.id.img_transition_light);
        btnOnOff = findViewById(R.id.btn_turn);

        btnOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnOn) {
                    imgTransitionLight.setImageResource(R.drawable.on);
                    turnOn = false;
                } else {
                    imgTransitionLight.setImageResource(R.drawable.off);
                    turnOn = true;
                }
            }
        });
    }
}
